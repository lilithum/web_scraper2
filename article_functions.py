from datetime import timedelta
import config
from bs4 import BeautifulSoup
import requests
import sys
from article_scraper import Article


def latest_news(latest_news_url, days_news):
    """get url with news for the last day"""
    requests_output = requests.get(latest_news_url)
    soup = BeautifulSoup(requests_output.content, 'html.parser')
    news_soup = soup.find_all(class_="fc-container__header js-container__header")
    link = news_soup[0].a["href"]
    link_date = news_soup[0].text.strip()
    days_news.extend(parse_date(link))
    return link_date


def date(given_date, days_news):
    """add articles to a list for a given day"""
    days_news.extend(parse_date(config.START_PAGE + '/{}/all'.format(given_date.strftime('%Y/%b/%d'))))


def period(start_date, end_date, days_news):
    """add articles from given period to a list"""
    min_day = min(start_date, end_date)
    max_day = max(start_date, end_date)
    min_day = min_day.date()
    max_day = max_day.date()
    delta = (max_day - min_day).days
    for day in range(delta + 1):
        current_day = min_day + timedelta(days=day)
        url = config.START_PAGE + '/{}/all'.format(current_day.strftime('%Y/%b/%d'))
        print(f"Collecting urls from {current_day.strftime('%Y/%b/%d')}")
        current_day_articles = parse_date(url)
        days_news.extend(current_day_articles)
    return days_news


def choose_article(days_news):
    """interactive interface for reading news"""
    for i, article in enumerate(days_news):
        print('[{}]'.format(i + 1), article[0])
    while True:
        article_id = input("Choose the number of the article or press 0 to exit: ")
        try:
            article_id = int(article_id)
        except TypeError:
            "Invalid input"
        if article_id == 0:
            sys.exit()
        article_url = days_news[article_id - 1][-1]
        if not article_url:
            print('Choose different article. This article is unavailable.')
            continue
        print_article(article_url)


def print_article(url):
    """Display article."""
    art = Article(url)
    print(art.title())
    print('        ' + art.description())
    date_time = art.date_time()
    print('Date: {:04d}-{:02d}-{:02d}'.format(date_time.year, date_time.month, date_time.day))
    print('Authors: ', end='')
    for i, author in enumerate(art.authors()):
        if i != 0:
            print(author, end='')
    print('\n***\n')
    for paragraph in art.body():
        print(paragraph)
    print('\n***')
    print("Topics are: ")
    for topic in art.topics():
        print(topic)


def parse_date(url):
    """parse articles for particular day"""
    soup = BeautifulSoup(requests.get(url).content, 'html.parser')
    article_soup = soup.body
    articles = article_soup.find_all('div', {'class': "fc-item__container"})
    headlines = []
    for article in articles:
        article_a = article.find('a', {'class': "u-faux-block-link__overlay js-headline-text"})
        headline = article_a.get_text().strip()
        link = article_a['href']
        if [headline, link] not in headlines:
            headlines.append([headline, link])
    return headlines
