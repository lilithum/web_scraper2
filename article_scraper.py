from bs4 import BeautifulSoup
import requests
from datetime import datetime


class Article(object):
    def __init__(self, url):
        self.url = url
        self._request = requests.get(self.url)
        self._soup = BeautifulSoup(self._request.content, 'html.parser')

    def is_request_ok(self):
        return self._request.status_code == requests.codes.ok

    def title(self):
        return self._soup.find_all("h1")[0].text[1:-1].strip()

    def description(self):
        description = self._soup.find_all(class_="content__standfirst")[0]
        return description.text.strip()

    def date_time(self):
        date_list = self._soup.find_all("time", attrs={"itemprop": "datePublished"})
        return datetime.fromtimestamp(int(date_list[0]['data-timestamp']) / 1000)

    def authors(self):
        authors = self._soup.find_all('a', attrs={'rel': 'author'})
        authors_with_urls = {}
        for author in authors:
            authors_with_urls[author.text.strip()] = author.get('href')
        authors = self._soup.find_all('p', attrs={"data-link-name": "byline", "class": "byline"})
        for author in authors:
            if not author.find('a'):
                author_name = author.text.strip()
                if author_name not in authors_with_urls.keys():
                    authors_with_urls[author_name] = 'None'
        return authors_with_urls

    def body(self):
        paragraphs = []
        paragraph_pre = self._soup.find(class_=["content__article-body from-content-api js-article__body",
                                                "from-content-api podcast__body"])
        try:
            text = paragraph_pre.find_all("p")
        except AttributeError:
            return
        num_paragraph = len(text)
        for i in range(num_paragraph):
            if str(text[i]).startswith("<p>"):
                paragraphs.append(text[i].text)
        return paragraphs

    def topics(self):
        topics_with_urls = {}
        topic_soup = self._soup.find_all(class_='submeta__link')
        for topic in topic_soup:
            curr_text = topic.text[1:-1]
            curr_url = topic.get('href')
            topics_with_urls[curr_text] = curr_url
        return topics_with_urls

    def main_topic(self):
        topic_soup = self._soup.find_all(class_='submeta__link')
        return topic_soup[0].text[1:-1]
