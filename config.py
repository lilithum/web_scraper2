# choose ACTION from [UPDATE, INITIATE]
ACTION = 'INITIATE'

# choose START date (if you choose INITIATE), FORMAT IS 'YYYY-MM-DD'
START = '2019-03-12'

# choose END date if you choose INITIATE), default will impute today's day, FORMAT IS 'YYYY-MM-DD'
END = '2019-03-12'

# choose DATABASE_NAME where to write or existing database to update. Does not overwrite
DATABASE_NAME = 'test6'

# choose starting url - only with news structured by date, like https://www.theguardian.com/world/middleeast
START_PAGE = r'https://www.theguardian.com/world/middleeast'

#your mysql server username
USERNAME = 'lucife'

#your mysql server password
PASSWORD = 'password'
