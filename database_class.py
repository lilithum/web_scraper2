from article_functions import *
from article_scraper import Article
import sys
import pymysql.cursors
import pymysql.err
from datetime import datetime
import json
import ast


class DataBase(object):
    """create and update database"""

    def __init__(self, username, password, db_name):
        self.username = username
        self.password = password
        self.db_name = db_name

    def is_connectable_mysql(self):
        """check connection to database"""
        try:
            cnx = pymysql.connect(host='localhost', user=self.username, password=self.password, charset='UTF8MB4')
        except pymysql.err.Error as e:
            print('Could not connect to mysql:\n', e)
            return False
        cnx.close()
        return True

    @staticmethod
    def valid_date(s):
        """return datetime date if date is of format YYYY-MM-DD."""
        try:
            return datetime.strptime(s, "%Y-%m-%d")
        except ValueError:
            msg = "Not a valid date in format YYYY-MM-DD: '{0}'.".format(s)
            print(msg)
            sys.exit(1)

    def initiate(self):
        """creates database and initialises all tables"""
        if not self.is_connectable_mysql():
            sys.exit(1)
        con = pymysql.connect(host='localhost', user=self.username, password=self.password, charset='UTF8MB4')

        try:
            with con.cursor() as cur:
                cur.execute(f"CREATE DATABASE {self.db_name}")
        except pymysql.err.ProgrammingError as error:
            code, message = error.args
            if code == 1007:
                yn = input(f"Database {self.db_name} exists. Delete? y/n: ")
                if yn.lower() == 'y':
                    with con.cursor() as cur:
                        cur.execute(f"DROP DATABASE {self.db_name}")
                        cur.execute(f"CREATE DATABASE {self.db_name}")
                else:
                    print('Exiting...')
                    sys.exit()
        try:
            with con.cursor() as cur:
                cur.execute(f"USE {self.db_name}")
                cur.execute('''CREATE TABLE articles (
                                            id INTEGER PRIMARY KEY AUTO_INCREMENT, 
                                            url VARCHAR (255),
                                            title VARCHAR (255),
                                            description BLOB,
                                            date_time DATETIME,
                                            date DATE,
                                            num_paragraphs INT,
                                            main_topic_id int,
                                            UNIQUE (url)
                                            ) DEFAULT CHARSET=UTF8MB4''')

                cur.execute('''CREATE TABLE authors (
                                        id INTEGER PRIMARY KEY AUTO_INCREMENT,
                                        name VARCHAR(255),
                                        url VARCHAR(255)
                                        ) DEFAULT CHARSET=UTF8MB4''')

                cur.execute('''CREATE TABLE article_authors (
                                        id INTEGER PRIMARY KEY AUTO_INCREMENT,
                                        article_id INT,
                                        author_num INT,
                                        author_id INT,
                                        FOREIGN KEY(author_id) REFERENCES authors(id),
                                        FOREIGN KEY(article_id) REFERENCES articles(id)
                                        ) DEFAULT CHARSET=UTF8MB4''')

                cur.execute('''CREATE TABLE topics (
                                        id INTEGER PRIMARY KEY AUTO_INCREMENT,
                                        topic VARCHAR(255),
                                        url VARCHAR(255),
                                        CONSTRAINT topics UNIQUE (topic,url)
                                        ) DEFAULT CHARSET=UTF8MB4''')

                cur.execute('''CREATE TABLE article_topics (
                                        id INTEGER PRIMARY KEY AUTO_INCREMENT,
                                        article_id INT,
                                        topic_num INT,
                                        topic_id INT,
                                        FOREIGN KEY(topic_id) REFERENCES topics(id),
                                        FOREIGN KEY(article_id) REFERENCES articles(id)
                                        ) DEFAULT CHARSET=UTF8MB4''')

                cur.execute('''CREATE TABLE text (
                                         article_id INT PRIMARY KEY,
                                         text MEDIUMTEXT,
                                         FOREIGN KEY(article_id) REFERENCES articles(id)
                                         ) DEFAULT CHARSET=UTF8MB4''')

                cur.execute('''CREATE TABLE exchange_rate (
                            date DATE,
                            IRR FLOAT,
                            IQD FLOAT,
                            ILS FLOAT,
                            JOD FLOAT,
                            LBP FLOAT,
                            SYP FLOAT,
                            TRY FLOAT,
                            AED FLOAT,
                            EGP FLOAT,
                            SAR FLOAT,
                            YER FLOAT,
                            OMR FLOAT,
                            QAR FLOAT,
                            BHD FLOAT,
                            KWD FLOAT
                            ) DEFAULT CHARSET=UTF8MB4''')

                con.commit()
        except pymysql.err.Error as e:
            print('An error occured while initiating a database: \n', e)
            if con:
                con.close()
            sys.exit()
        con.close()

    def update(self, cur, article, con):
        """add a single article to database"""
        # Check if the article exists in the articles table.
        url_short = article.url.replace('https://www.theguardian.com', '')
        cur.execute(f'SELECT * from articles WHERE url = "{url_short}"')
        if cur.fetchone():
            return

        # article table
        sql_query = f"""INSERT INTO articles VALUES (NULL, %s, %s,  %s, %s, %s, %s, NULL)"""

        sql_values = (pymysql.escape_string(url_short), pymysql.escape_string(article.title()),
                      pymysql.escape_string(article.description()), article.date_time(),
                      article.date_time().strftime("%Y-%m-%d"),
                      len(article.body()))
        cur.execute(sql_query, sql_values)

        # Article_id
        cur.execute("""SELECT MAX(id) FROM articles""")
        article_id = cur.fetchone()[0]

        # Update authors table and article_authors table
        for i, name in enumerate(article.authors().keys()):
            short_url_auth = (article.authors()[name]).replace('https://www.theguardian.com', '')
            author_id_query_raw = """SELECT authors.id 
                                    FROM authors 
                                    WHERE authors.name = "{name}" AND authors.url = '{url}'"""
            author_id_query = author_id_query_raw.format(name = pymysql.escape_string(name),
                                                         url=pymysql.escape_string(short_url_auth))
            cur.execute(author_id_query)
            result = cur.fetchone()

            if not result:
                cur.execute(f"INSERT INTO authors VALUES (NULL, %s, %s)",
                            (name, pymysql.escape_string(short_url_auth)))
                con.commit()
                cur.execute(author_id_query)
                author_id = cur.fetchone()[0]

            else:
                author_id = result[0]

            cur.execute(f"INSERT INTO article_authors VALUES (NULL,'{article_id}',{i},'{author_id}')")

        # Update topics table and article_topics table
        for i, topic in enumerate(article.topics().keys()):
            topic_short_url = article.topics()[topic].replace('https://www.theguardian.com', '')
            topic_short_url = pymysql.escape_string(topic_short_url)
            topic_query = f"""SELECT id FROM topics WHERE topic='{pymysql.escape_string(
                topic)}' AND url='{topic_short_url}'"""

            cur.execute(topic_query)
            result = cur.fetchone()
            if not result:
                cur.execute(
                    f"""INSERT INTO topics VALUES (NULL,'{pymysql.escape_string(topic)}','{topic_short_url}')""")
                cur.execute(topic_query)
                topic_id = cur.fetchone()[0]
            else:
                topic_id = result[0]

            cur.execute(f"INSERT INTO article_topics VALUES (NULL,'{article_id}',{i},'{topic_id}')")

        # Update main_topic_id to the article table
        topic_query = f"""SELECT id FROM topics WHERE topic='{pymysql.escape_string(article.main_topic())}'"""
        cur.execute(topic_query)
        main_topic_id = cur.fetchone()[0]
        cur.execute(f"""UPDATE articles SET main_topic_id = '{main_topic_id}' WHERE id = {article_id}""")

        # Update text table
        text = '\n'.join(article.body())
        text = pymysql.escape_string(text)
        cur.execute(f"""INSERT INTO text VALUES ({article_id}, '{text}')""")

        # Update exchange_rate table
        query = (f"""SELECT * FROM exchange_rate 
                         WHERE date='{(article.date_time()).strftime('%Y-%m-%d')}'""")
        cur.execute(query)
        exist = cur.fetchone()
        if not exist:
            currencies = self.get_currency(article.date_time().strftime('%Y-%m-%d'))
            date_ = (article.date_time()).strftime("%Y-%m-%d")
            cur.execute(f"""INSERT INTO exchange_rate VALUES (
                            '{date_}',
                            {currencies['IRR']},
                            {currencies['IQD']},
                            {currencies['ILS']},
                            {currencies['JOD']},
                            {currencies['LBP']},
                            {currencies['SYP']},
                            {currencies['TRY']},
                            {currencies['AED']},
                            {currencies['EGP']},
                            {currencies['SAR']},
                            {currencies['YER']},
                            {currencies['OMR']},
                            {currencies['QAR']},
                            {currencies['BHD']},
                            {currencies['KWD']}
                            )""")

    @staticmethod
    def get_currency(date):
        countries_currency = {'Iran': 'IRR', 'Iraq': 'IQD', 'Israel': 'ILS', 'Jordan': 'JOD', 'Lebanon': 'LBP',
                              'Syria': 'SYP', 'Turkey': 'TRY',
                              'United Arab Emirates': 'AED', 'Egypt': 'EGP', 'Saudi Arabia': 'SAR', 'Yemen': 'YER',
                              'Oman': 'OMR', 'Qatar': 'QAR', 'Bahrain': 'BHD', 'Kuwait': 'KWD'}
        response = requests.get(
            f"""https://openexchangerates.org/api/historical/{date}.json?app_id=99059243641f462b8b426906003766bc""")
        if response.status_code == 200:
            currency_dict = ast.literal_eval(json.dumps(json.loads(response.text)))['rates']
            currencies = {}
            for key in countries_currency.values():
                currencies[key] = currency_dict[key]
        else:
            currencies = dict.fromkeys(countries_currency.values(), 0)
        return currencies

    def current_day(self):
        end_no_format = latest_news(config.START_PAGE, [])
        end = datetime.strptime(end_no_format, '%d %B %Y')
        return end

    def collect_data(self, start, end):
        """Collect articles"""
        headlines = period(start, end, [])
        articles = []
        for link in headlines:
            article = Article(link[1])
            print(f"Collecting article from {link[1]}")
            if article.is_request_ok() and article.body():
                articles.append(article)
        return articles

    def last_date(self):
        """find last date for which news are in the database"""
        if not self.is_connectable_mysql():
            sys.exit()
        con = pymysql.connect(host='localhost', user=self.username, password=self.password, db=self.db_name, charset='UTF8MB4')
        try:
            with con.cursor() as cur:
                cur.execute('''SELECT MAX(date_time)
                                    FROM articles
                    ''')
                start_date = cur.fetchone()[0]
        except pymysql.err.Error:
            print('SQL error while trying to query database!')
            sys.exit()
        finally:
            con.close()
        return start_date

    def update_all(self, articles):
        """write all new articles to database"""
        if not self.is_connectable_mysql():
            sys.exit(1)
        if not articles:
            return
        con = pymysql.connect(host='localhost', user=self.username, password=self.password,
                              database=self.db_name, charset='UTF8MB4')
        for i, article in enumerate(articles):
            try:
                with con.cursor() as cur:
                    self.update(cur, article, con)
                    if (i + 1) % 10 == 0:
                        con.commit()
                    con.commit()
            except Exception as e:
                print(article.url)
                print('An error occured while trying to write to database:\n', e)
                print('Writing next article...')
        con.close()
        return