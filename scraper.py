from utils import *
import config
from database_class import DataBase


def database(action, start='', end='', username='', password='', db_name='guardian_base'):
    """build database with parameters from config.py of cli input"""
    if action not in ['UPDATE', 'INITIATE']:
        print("Invalid config file!")
        sys.exit(0)
    db = DataBase(username, password, db_name)
    articles = list()
    if action == 'INITIATE':
        db.initiate()
    elif action == 'UPDATE':
        if start == '':
            start = db.last_date()
        if end == '':
            end = db.current_day()
    if type(start) == str:
        start = db.valid_date(start)
    if type(end) == str:
        end = db.valid_date(end)

    min_day = min(start, end)
    max_day = max(start, end)
    min_day_date = min_day.date()
    max_day_date = max_day.date()
    delta = (max_day_date - min_day_date).days
    for day in range(delta + 1):
        try:
            current_day = min_day + timedelta(days=day)
            articles = db.collect_data(current_day, current_day)
            db.update_all(articles)
        except Exception as e:
            print(f'An error occured while writing articles from day {current_day}: \n{e}')
            continue




def choose_action(args):
    """Choose function according to command line args"""
    days_news = []
    if args.latest_news:
        latest_news(config.START_PAGE, days_news)

    if args.date:
        date(args.date, days_news)

    if args.period:
        period(args.period[0], args.period[1], days_news)

    if args.article:
        print_article(args.article)

    if args.commands:
        username = input("mysql username: ")
        password = input("mysql password: ")
        if username and password:
            database(args.commands.upper(), args.start_date, args.end_date, username, password, args.db_name)
        else:
            print('Invalid credentials.')
            sys.exit(0)
    if len(days_news) > 0:
        choose_article(days_news)


def main():
    args = parse_arguments()
    choose_action(args)


if __name__ == "__main__":
    if len(sys.argv) > 1:
        print("Command line arguments...")
        main()
    else:
        print("Config arguments...")
        database(config.ACTION, config.START, config.END, config.USERNAME, config.PASSWORD, config.DATABASE_NAME)
