"""This is an util module for scraper project. It parses and validates command line input
"""

import argparse
from article_functions import *
from datetime import datetime


def parse_arguments():
    """Parse command line arguments"""
    parser = argparse.ArgumentParser()

    group = parser.add_mutually_exclusive_group()
    group.add_argument('-l', '--latest_news', help="Get latest news", action='store_true')
    group.add_argument('-d', '--date', type=valid_date, help="Particular day - format YYYY-MM-DD")
    group.add_argument('-a', '--article', type=valid_link, help="Show contents of the linked article")
    group.add_argument('-p', '--period', type=valid_date, nargs=2, metavar=('date-from', 'date-to'),
                       help="Show articles from date 1 to date 2")

    subparsers = parser.add_subparsers(help='commands', dest='commands')
    update_parser = subparsers.add_parser('update', help='Update existing database')
    update_parser.add_argument('db_name', action='store', type=str, help="update existing datebase")
    initiate_parser = subparsers.add_parser('initiate', help='Initiate database')
    initiate_parser.add_argument('db_name', action='store', type=str, help='name of the new database')
    initiate_parser.add_argument('start_date', action='store', type=valid_date, help='start_date to new file')
    initiate_parser.add_argument('end_date', action='store', type=valid_date, default='', help='start_date to new file')

    args = parser.parse_args()
    return args


def valid_link(url):
    """type of the valid link"""
    if requests.get(url).status_code == requests.codes.ok:
        return url
    else:
        msg = "Not a valid link: '{0}'.".format(url)
        raise argparse.ArgumentTypeError(msg)


def valid_date(s):
    """return datetime date if date is of valid format."""
    try:
        return datetime.strptime(s, "%Y-%m-%d")
    except ValueError:
        msg = "Not a valid date in format YYYY-MM-DD: '{0}'.".format(s)
        raise argparse.ArgumentTypeError(msg)
